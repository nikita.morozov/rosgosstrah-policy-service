const webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const chromium = require('chromium');
require('chromedriver');

const { By, until, Key } = webdriver;

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const { default: PQueue } = require('p-queue');
const FillingQueue = new PQueue({ concurrency: 1 });
const CheckingStatusQueue = new PQueue({ concurrency: 2 });
const SendingLinkQueue = new PQueue({ concurrency: 1 });

const axios = require('axios');

const database = require('quick.db');
const taskDB = new database.table('Tasks');
const policyDataDB = new database.table('PolicyData');
const policyWaitingQueueDB = new database.table('PolicyWaitingQueue');

let driver;
const port = 3939;
const appName = 'RosGosStrah Policy Service';
const AuthURL = 'https://tarif.rgs.ru/ekis/tariff';

const ElementIs = {
  Enabled: 'Enabled',
  Disabled: 'Disabled',
  Visible: 'Visible',
  NotVisible: 'NotVisible',
  Selected: 'Selected',
  NotSelected: 'NotSelected'
};

const {
  Sleep, WaitWhileCheckTitle, WaitWhileNotDisplayed, ClickingInDOM, WriteDataInDOM, WriteData, WriteDataAndPressEnter, ClearAndWriteData,
  ClickingAfterWaiting, ClearAndWriteDataAfterWaiting, ClickAndWriteData
} = require('./helpers');

const LoginFieldXpath = `//*[contains(@id, 'login_form:username::content')]`;
const LogoutFieldXpath = `//*[contains(@id, 'pt1_:pt_logout_btn')]`;
const PassFieldXpath = `//*[contains(@id, 'login_form:password::content')]`;
const EnterButtonXpath = `//*[contains(@id, 'login_form:login_btn')]`;

const ConfirmEnterButtonXpath = `//*[contains(@id, 'pt1_:close')]`;
const OSAGOButtonXpath = `//*[contains(@id, 'pt1_:new_osagong_btn')]`;

const NotBCategoryButtonXpath = `//*[contains(@id, 'pt1_:tsdk_no_need_btn')]`;

const VINFieldXpath = `//*[contains(@id, 'pt1_:ident_no_tsdk::content')]`;
const BodyNumberFieldXpath = `//*[contains(@id, 'pt1_:body_no_tsdk::content')]`;
const DataRequestButtonXpath = `//*[contains(@id, 'pt1_:firstReqTsdk')]`;

const StartPolicyDateFieldXpath = `//*[contains(@id, 'pt1_:con_begin_date::content')]`;
const WriteCityFieldXpath = `//*[contains(@id, 'pt1_:place_::content')]`;
const ChangeWindowXpath = `//*[contains(@id, 'pt1_:place__afrLovInternalTableId::db')]`;
const ChangeCityXpath = `//*[contains(@id, 'pt1_:place__afrLovInternalTableId::db')]//*[contains(@class,'xzy')]//*[contains(text(), 'Львовка')]/parent::*/parent::*//*[contains(text(), 'Омская')]`;
const AcceptCityButtonXpath = `//*[contains(@id, 'pt1_:place__afrLovDialogId::ok')]`;

const TSMarkFieldXpath = `//*[contains(@id, 'pt1_:markNameId2::content')]`;
const TSSelectMarkXpath = `//*[contains(@id, 'pt1_:markNameId2::')]//*[contains(text(), 'BMW')]`;
const TSModelFieldXpath = `//*[contains(@id, 'pt1_:modelNameId2::content')]`;
const TSSelectModelXpath = `//*[contains(@id, 'pt1_:modelNameId2::')]//*[contains(text(), '125')]`;
const TSUsingTrailerCheckboxXpath = `//*[contains(@id, 'pt1_:with_trailer')]//*[contains(text(),'прицеп')]`;
const TSReleaseYearFieldXpath = `//*[contains(@id, 'pt1_:create_year::content')]`;
const TSUsingTargetXpath = `//*[contains(@id, 'pt1_:use_ts::content')]//*[contains(text(),'личная')]`;
const TSPowerFieldXpath = `//*[contains(@id, 'pt1_:power::content')]`;
const TSUsingPeriodXpath = `//*[contains(@id, 'pt1_:use_period::content')]//*[contains(text(),'10 месяцев и более')]`;
const InfinityDriversXpath = `//*[contains(@id, 'pt1_:usages::content')]//*[contains(text(),'Без ограничений')]`;
const AddDriverBTNXpath = `//*[contains(@id, 'pt1_:usage_add')]`;
const DriverSurnameFieldXpath = `//*[contains(@id, 'pt1_:frm_limit_fam::content')]`;
const DriverFirstNameFieldXpath = `//*[contains(@id, 'pt1_:frm_limit_nam::content')]`;
const DriverSecondNameFieldXpath = `//*[contains(@id, 'pt1_:frm_limit_otch::content')]`;
const DriverBirthdayFieldXpath = `//*[contains(@id, 'pt1_:frm_limit_dr::content')]`;
const DriverFirstDLIssueDateFieldXpath = `//*[contains(@id, 'pt1_:frm_limit_ds::content')]`;
const DriverGenderCheckerXpath = `//*[contains(@id, 'pt1_:frm_limit_sex::content')]`;
const DriverGenderXpath = `//*[contains(@id, 'pt1_:frm_limit_sex::content')]//*[contains(text(),'М')]`;
const DriverDLSeriesFieldXpath = `//*[contains(@id, 'pt1_:frm_limit_ser::content')]`;
const DriverDLNumberFieldXpath = `//*[contains(@id, 'pt1_:frm_limit_num::content')]`;
const AddToDriversListBTNXpath = `//*[contains(@id, 'pt1_:cb2')]`;

const CalcBTNXpath = `//*[contains(@id, 'pt1_:make_calc')]`;
const CalcMsgWindowXpath = `//*[contains(@id, 'd1::msgDlg::_cnt')]//*[contains(text(),'Сделан расчет')]`;
const CloseMsgWindowXpath = `//*[contains(@id, 'd1::msgDlg::cancel')]`;
const PolicyCostXpath = `//*[contains(@id, 'pt1_:tar_itog::content')]`;
const ContractBTNXpath = `//*[contains(@id, 'pt1_:make_con')]`;

const InsurerSurnameXpath = `//*[contains(@id, 'pt1_:condata_ins_fm::content')]`;
const InsurerFirstNameXpath = `//*[contains(@id, 'pt1_:condata_ins_nm::content')]`;
const InsurerSecondNameXpath = `//*[contains(@id, 'pt1_:condata_ins_pm::content')]`;
const InsurerBirthdayXpath = `//*[contains(@id, 'pt1_:condata_ins_bd::content')]`;
const InsurerGenderXpath = `//*[contains(@id, 'pt1_:condata_ins_sex::content')]`;
const InsurerSelectMaleGenderXpath = `//*[contains(@id, 'pt1_:condata_ins_sex::content')]//*[contains(text(), 'мужской')]`;
const InsurerSelectFemaleGenderXpath = `//*[contains(@id, 'pt1_:condata_ins_sex::content')]//*[contains(text(), 'женский')]`;
const InsurerDocSeriesXpath = `//*[contains(@id, 'pt1_:condata_ins_doc_ser::content')]`;
const InsurerDocNumberXpath = `//*[contains(@id, 'pt1_:condata_ins_doc_num::content')]`;
const InsurerDocIssueDateXpath = `//*[contains(@id, 'pt1_:condata_ins_doc_when::content')]`;
const InsurerSelectRegAddressButtonXpath = `//*[contains(@id, 'pt1_:condata_ins_adr_add')]//*[contains(text(), 'АДРЕС')]`;
const InsurerPhoneXpath = `//*[contains(@id, 'pt1_:condata_ins_ph1::content')]`;
const InsurerEmailXpath = `//*[contains(@id, 'pt1_:condata_ins_em1::content')]`;

const InsurerItsOwnerXpath = `//*[contains(@id, 'pt1_:insurant_same_as_owner_checkbox::content')]`;

const SelectOwnerDataEqualsXpath = `//*[contains(@id, 'pt1_:condata_own_copy::content')]`;
const OwnerDataIsIndividualXpath = `//*[contains(@id, 'pt1_:condata_own_copy::content')]//*[text()=' ']`;
const OwnerDataEqualInsurerDaraXpath = `//*[contains(@id, 'pt1_:condata_own_copy::content')]//*[contains(text(), 'Cтрахователь')]`;
const OwnerDataEqualDriverDaraXpath = (id) => `//*[contains(@id, 'pt1_:condata_own_copy::content')]//*[contains(text(), 'Водитель №${id}')]`;

const OwnerSurnameXpath = `//*[contains(@id, 'pt1_:condata_own_fm::content')]`;
const OwnerFirstNameXpath = `//*[contains(@id, 'pt1_:condata_own_nm::content')]`;
const OwnerSecondNameXpath = `//*[contains(@id, 'pt1_:condata_own_pm::content')]`;
const OwnerBirthdayXpath = `//*[contains(@id, 'pt1_:condata_own_bd::content')]`;
const OwnerGenderXpath = `//*[contains(@id, 'pt1_:condata_own_sex::content')]`;
const OwnerSelectMaleGenderXpath = `//*[contains(@id, 'pt1_:condata_own_sex::content')]//*[contains(text(), 'мужской')]`;
const OwnerSelectFemaleGenderXpath = `//*[contains(@id, 'pt1_:condata_own_sex::content')]//*[contains(text(), 'женский')]`;
const OwnerDocSeriesXpath = `//*[contains(@id, 'pt1_:condata_own_doc_ser::content')]`;
const OwnerDocNumberXpath = `//*[contains(@id, 'pt1_:condata_own_doc_num::content')]`;
const OwnerDocIssueDateXpath = `//*[contains(@id, 'pt1_:condata_own_doc_when::content')]`;
const OwnerSelectRegAddressButtonXpath = `//*[contains(@id, 'pt1_:condata_own_adr_add')]//*[contains(text(), 'АДРЕС')]`;
const OwnerCityFieldXpath = `//*[contains(@id, 'condata_adr_place_name::content')]`;
const OwnerStreetFieldXpath = `//*[contains(@id, 'condata_adr_street_name::content')]`;
const OwnerCityOkBTNXpath = `//*[contains(@id, 'cb10')][contains(text(),'Да')]`;
const OwnerPhoneXpath = `//*[contains(@id, 'pt1_:condata_own_ph1::content')]`;
const OwnerEmailXpath = `//*[contains(@id, 'pt1_:condata_own_em1::content')]`;

const CardCityFieldXpath = `//*[contains(@id, 'condata_adr_place_name::content')]`;
const CardCityYesBTNXpath = `//*[contains(@id, 'cb10')][contains(text(),'Да')]`;
const SelectCityFromListXpath = `//*[contains(@id, 'condata_adr_place_name_afrLovInternalTableId::db')]//*[contains(@class,'xzv')]//*[contains(text(), 'Львовка')]/parent::*/parent::*//*[contains(text(), 'Омская')]`;
const SelectedCityOkBTNXpath = `//*[contains(@id, 'condata_adr_place_name_afrLovDialogId::ok')]`;
const SelectStreetWindowXpath = `//*[contains(@id, 'condata_adr_street_name::lovIconId')]`;
const CardStreetFieldXpath = `//*[contains(@id, 'condata_adr_street_name_afrLovInternalQueryId:val00::content')]`;
const SelectStreetFromListXpath = `//*[contains(@id, 'condata_adr_street_name_afrLovInternalTableId::db')]//*[contains(text(), 'Победная')]`;
const SelectedStreetOkBTNXpath = `//*[contains(@id, 'condata_adr_street_name_afrLovDialogId::ok')]`;

const NextTabButtonXpath = `//*[contains(@id, 'pt1_:condata_next')]`;

const ChangeDocTypeXpath = `//*[contains(@id, 'pt1_:condata_ts_cert_name::content')]`;
const TypePTSXpath = `//*[contains(@id, 'pt1_:condata_ts_cert_name::content')]//*[text()='Паспорт ТС']`;
const TypeEPTSXpath = `//*[contains(@id, 'pt1_:condata_ts_cert_name::content')]//*[text()='Электронный Паспорт ТС']`;
const DocSeriesFieldXpath = `//*[contains(@id, 'pt1_:condata_ts_doc_ser::content')]`;
const DocNumberFieldXpath = `//*[contains(@id, 'pt1_:condata_ts_doc_num::content')]`;
const DocIssueDateFieldXpath = `//*[contains(@id, 'pt1_:condata_ts_doc_when::content')]`;

async function Authorization (driver, id) {
  await console.log('AUTHORIZATION');
  await driver.get(AuthURL);
  await ClearAndWriteData(driver, LoginFieldXpath, 'WEB_RABITKASH');
  await ClearAndWriteData(driver, PassFieldXpath, 'Rich!171');
  await ClickingAfterWaiting(driver, EnterButtonXpath, ElementIs.Enabled);
}

async function GoToFilling (driver, id) {
  await console.log('GO TO FILLING');
  await ClickingAfterWaiting(driver, ConfirmEnterButtonXpath, ElementIs.Enabled);
  await ClickingAfterWaiting(driver, OSAGOButtonXpath, ElementIs.Enabled);
}

async function Filling (driver, id) {
  await console.log('FILLING');

  await WaitWhileNotDisplayed(driver, VINFieldXpath);

  await ClearAndWriteData(driver, VINFieldXpath, 'WBAUC31010VF43630');
  await ClearAndWriteData(driver, BodyNumberFieldXpath, 'WBAUC31010VF43630');
  await ClickingAfterWaiting(driver, DataRequestButtonXpath, ElementIs.Enabled);

  await WaitWhileNotDisplayed(driver, StartPolicyDateFieldXpath);

  await ClearAndWriteData(driver, StartPolicyDateFieldXpath, '25.12.2019');
  await driver.sleep(500);
  await driver.findElement(By.xpath(WriteCityFieldXpath)).click();
  await driver.sleep(1000);
  await WriteDataAndPressEnter(driver, WriteCityFieldXpath, 'Львовка');
  await driver.sleep(500);
  await driver.wait(webdriver.until.elementLocated(By.xpath(ChangeCityXpath))).then(async element => {
    await driver.executeScript('arguments[0].click()', await element);
    await ClickingInDOM(driver, AcceptCityButtonXpath);
  });

  await driver.sleep(750);
  await ClearAndWriteData(driver, TSMarkFieldXpath, 'BMW');
  await driver.wait(webdriver.until.elementLocated(By.xpath(TSSelectMarkXpath))).then(async () => {
    await ClickingAfterWaiting(driver, TSSelectMarkXpath, ElementIs.Enabled);
  });
  await driver.sleep(1000);
  await WriteData(driver, TSModelFieldXpath, '125');
  await driver.sleep(1000);
  await driver.wait(webdriver.until.elementLocated(By.xpath(TSSelectModelXpath))).then(async () => {
    await ClickingAfterWaiting(driver, TSSelectModelXpath, ElementIs.Enabled);
  });

  await ClearAndWriteData(driver, TSReleaseYearFieldXpath, '2008');
  await ClearAndWriteData(driver, TSPowerFieldXpath, '218');

  for (let i = 0; i < 1; i++) {
    await ClickingAfterWaiting(driver, AddDriverBTNXpath, ElementIs.Enabled);

    await driver.sleep(1000);
    await WaitWhileNotDisplayed(driver, DriverSurnameFieldXpath);
    await driver.sleep(500);

    await ClearAndWriteData(driver, DriverSurnameFieldXpath, 'Турбас');
    await ClearAndWriteData(driver, DriverFirstNameFieldXpath, 'Ярослав');
    await ClearAndWriteData(driver, DriverSecondNameFieldXpath, 'Викторович');
    await ClearAndWriteData(driver, DriverBirthdayFieldXpath, '10.11.1986');
    await ClearAndWriteData(driver, DriverFirstDLIssueDateFieldXpath, '24.04.2012');
    await ClickingAfterWaiting(driver, DriverGenderCheckerXpath, ElementIs.Enabled);
    await ClickingAfterWaiting(driver, DriverGenderXpath, ElementIs.Enabled);
    await ClearAndWriteData(driver, DriverDLSeriesFieldXpath, '5909');
    await ClearAndWriteData(driver, DriverDLNumberFieldXpath, '179035');

    await ClickingAfterWaiting(driver, AddToDriversListBTNXpath, ElementIs.Enabled);
  }

  await ClickingInDOM(driver, CalcBTNXpath);

  await driver.wait(webdriver.until.elementLocated(By.xpath(CalcMsgWindowXpath))).then(async () => {
    await WaitWhileNotDisplayed(driver, CalcMsgWindowXpath);
  });

  await ClickingAfterWaiting(driver, CloseMsgWindowXpath, ElementIs.Enabled);

  let policyCost = await driver.findElement(By.xpath(PolicyCostXpath)).getAttribute('value');
  await console.log('POLICY COST: ', policyCost);

  await ClickingAfterWaiting(driver, ContractBTNXpath, ElementIs.Enabled);

  await driver.wait(webdriver.until.elementLocated(By.xpath(InsurerSurnameXpath)));
  await WaitWhileNotDisplayed(driver, InsurerSurnameXpath);
  await ClearAndWriteData(driver, InsurerSurnameXpath, 'Турбас');
  await ClickAndWriteData(driver, InsurerFirstNameXpath, 'Ярослав', 750);
  await ClickAndWriteData(driver, InsurerSecondNameXpath, 'Викторович', 750);
  await ClearAndWriteData(driver, InsurerBirthdayXpath, '10.11.1986');
  await ClickingAfterWaiting(driver, InsurerGenderXpath, ElementIs.Enabled);
  await ClickingAfterWaiting(driver, InsurerSelectMaleGenderXpath, ElementIs.Enabled);
  await ClearAndWriteData(driver, InsurerDocSeriesXpath, '5206');
  await ClearAndWriteData(driver, InsurerDocNumberXpath, '416484');
  await ClearAndWriteData(driver, InsurerDocIssueDateXpath, '27.02.2007');

  await SelectPersonRegAddress(driver, InsurerSelectRegAddressButtonXpath, 'Львовка', 'Победная');

  await WaitWhileNotDisplayed(driver, InsurerPhoneXpath);
  await ClickingAfterWaiting(driver, InsurerPhoneXpath, ElementIs.Enabled);
  await driver.sleep(500);
  await WriteData(driver, InsurerPhoneXpath, '9055868858');
  await ClearAndWriteData(driver, InsurerEmailXpath, 'email@mail.ru');

  await ClickingAfterWaiting(driver, SelectOwnerDataEqualsXpath, ElementIs.Enabled);
  await ClickingAfterWaiting(driver, OwnerDataIsIndividualXpath, ElementIs.Enabled);

  await driver.wait(webdriver.until.elementLocated(By.xpath(OwnerSurnameXpath)));
  await WaitWhileNotDisplayed(driver, OwnerSurnameXpath);
  await ClearAndWriteData(driver, OwnerSurnameXpath, 'Турбас');
  await ClearAndWriteData(driver, OwnerFirstNameXpath, 'Ярослав');
  await ClearAndWriteData(driver, OwnerSecondNameXpath, 'Викторович');
  await ClearAndWriteData(driver, OwnerBirthdayXpath, '10.11.1986');
  await ClickingAfterWaiting(driver, OwnerGenderXpath, ElementIs.Enabled);
  await ClickingAfterWaiting(driver, OwnerSelectMaleGenderXpath, ElementIs.Enabled);
  await ClearAndWriteData(driver, OwnerDocSeriesXpath, '5206');
  await ClearAndWriteData(driver, OwnerDocNumberXpath, '416484');
  await ClearAndWriteData(driver, OwnerDocIssueDateXpath, '27.02.2007');

  await SelectPersonRegAddress(driver, OwnerSelectRegAddressButtonXpath, 'Львовка', 'Победная');

  await ClearAndWriteData(driver, OwnerPhoneXpath, '9055868858');
  await ClearAndWriteData(driver, OwnerEmailXpath, 'email@mail.ru');

  await ClickingAfterWaiting(driver, NextTabButtonXpath, ElementIs.Enabled);

  await WaitWhileNotDisplayed(driver, ChangeDocTypeXpath);
  await ClickingAfterWaiting(driver, ChangeDocTypeXpath, ElementIs.Enabled);
  await WaitWhileNotDisplayed(driver, TypePTSXpath);
  await ClickingAfterWaiting(driver, TypePTSXpath, ElementIs.Enabled);
  await ClearAndWriteData(driver, DocSeriesFieldXpath, '1234');
  await ClearAndWriteData(driver, DocSeriesFieldXpath, '567899');
  await ClearAndWriteData(driver, DocIssueDateFieldXpath, '11.02.2019');
}

async function SelectPersonRegAddress (driver, Button, CityValue, StreetValue) {
  await ClickingAfterWaiting(driver, Button, ElementIs.Enabled);

  await WaitWhileNotDisplayed(driver, InsurerSelectRegAddressButtonXpath);
  await WriteDataAndPressEnter(driver, CardCityFieldXpath, CityValue);
  await driver.sleep(500);
  await driver.wait(webdriver.until.elementLocated(By.xpath(SelectCityFromListXpath))).then(async element => {
    await driver.executeScript('arguments[0].click()', await element);
    await ClickingInDOM(driver, SelectedCityOkBTNXpath);
  });

  await WaitWhileNotDisplayed(driver, SelectStreetWindowXpath);
  await ClickingAfterWaiting(driver, SelectStreetWindowXpath, ElementIs.Enabled);
  await WaitWhileNotDisplayed(driver, CardStreetFieldXpath);
  await WriteDataAndPressEnter(driver, CardStreetFieldXpath, StreetValue);
  await driver.sleep(500);
  await WaitWhileNotDisplayed(driver, SelectStreetFromListXpath);
  await driver.wait(webdriver.until.elementLocated(By.xpath(SelectStreetFromListXpath))).then(async element => {
    await driver.executeScript('arguments[0].click()', await element);
    await ClickingInDOM(driver, SelectedStreetOkBTNXpath);
  });
  await WaitWhileNotDisplayed(driver, CardCityYesBTNXpath);
  await ClickingInDOM(driver, CardCityYesBTNXpath);
}

async function SaveDataInDB (id, email, ReqBody) {
  await console.log('SAVING DATA IN DB, DATA: ', ReqBody);
  let userData = await policyDataDB.get(`data${id}`);
  if (!userData) {
    const { login, password } = ReqBody;
    const { manager, signatory } = ReqBody;
    const { insurerSurname, insurerFirstName, insurerSecondName, insurerBirthday, insurerGender, insurerDocType, insurerDocSeries, insurerDocNumber, insurerDocIssueDate, insurerRegion, insurerCity, insurerPhone, insurerEmail } = ReqBody;
    const { ownerSurname, ownerFirstName, ownerSecondName, ownerBirthday, ownerGender, ownerDocType, ownerDocSeries, ownerDocNumber, ownerDocIssueDate, ownerRegion, ownerCity, ownerPhone } = ReqBody;
    const { carMark, carModel, carPower, carReleaseYear, usingTarget, usingWithTrailer } = ReqBody;
    const { tsDocType, tsDocSeriesNumber, tsIssueDate, tsGosNumber, tsBodyNumber, tsChassisNumber, tsVIN, diagCardSeriesNumber, diagCardIssueDate, diagCardFinishDate } = ReqBody;
    const { infinityDriversCount, drivers } = ReqBody;

    let croppedInsurerRegion;
    let croppedOwnerRegion;
    if (insurerRegion !== insurerCity) {
      // croppedInsurerRegion = await CropRegion(insurerRegion);
      // let croppedInsurerCity = await CropCity(insurerCity);
    }
    if (ownerRegion !== ownerCity) {
      // croppedOwnerRegion = await CropRegion(ownerRegion);
      // let croppedOwnerCity = await CropCity(ownerCity);
    }

    await policyDataDB.set(`data${id}`, {
      auth: {
        login: login,
        password: password
      },
      agent: {
        manager: manager,
        signatory: signatory
      },
      insurer: {
        surname: insurerSurname,
        firstName: insurerFirstName,
        secondName: insurerSecondName,
        birthday: insurerBirthday,
        gender: insurerGender,
        docType: insurerDocType,
        docSeries: insurerDocSeries,
        docNumber: insurerDocNumber,
        docIssueDate: insurerDocIssueDate,
        region: (insurerRegion !== insurerCity) ? croppedInsurerRegion : insurerRegion,
        city: (insurerRegion !== insurerCity) ? insurerCity : '',
        phone: insurerPhone,
        email: email
      },
      owner: {
        surname: ownerSurname,
        firstName: ownerFirstName,
        secondName: ownerSecondName,
        birthday: ownerBirthday,
        gender: ownerGender,
        docType: ownerDocType,
        docSeries: ownerDocSeries,
        docNumber: ownerDocNumber,
        docIssueDate: ownerDocIssueDate,
        region: (ownerRegion !== ownerCity) ? croppedOwnerRegion : ownerRegion,
        city: (ownerRegion !== ownerCity) ? ownerCity : '',
        phone: ownerPhone,
      },
      transport: {
        mark: carMark,
        model: carModel,
        power: carPower,
        releaseYear: carReleaseYear,
      },
      tsDocs: {
        usingTarget: usingTarget,
        usingWithTrailer: usingWithTrailer,
        type: tsDocType,
        seriesNumber: tsDocSeriesNumber,
        issueDate: tsIssueDate,
        gosNumber: tsGosNumber,
        bodyNumber: tsBodyNumber,
        chassisNumber: tsChassisNumber,
        vinNumber: tsVIN,
        DCSeriesNumber: diagCardSeriesNumber,
        DCIssueDate: diagCardIssueDate,
        DCFinishDate: diagCardFinishDate
      },
      infinityDriversCount: infinityDriversCount,
      drivers: (!infinityDriversCount) ? await new Promise(async (resolve) => { await resolve(drivers); }) : []
    });
    await console.log(`DATA WITH ID: ${id} IS SAVED`);
  } else {
    await console.log(`THIS ID: ${id} IS ALREADY ID THE DATABASE`);
  }
}

/**
 * @return {string}
 */
function CropRegion (region) {
  console.log('REGION: ', region);

  let cutRegion;
  if (region.includes('обл')) {
    cutRegion = region.replace('обл', '').trim();
  } else if (region.includes('Респ')) {
    cutRegion = region.replace('Респ', '').trim();
  } else if (region.includes('край')) {
    cutRegion = region.replace('край', '').trim();
  } else if (region.includes('АО')) {
    cutRegion = region.replace('АО', '').trim();
  } else if (region.includes('г')) {
    cutRegion = region.replace('г', '').trim();
  } else {
    console.log('NOT FOUND REGION');
    return;
  }
  console.log('OLD VALUE: ' + region);
  console.log('NEW VALUE: ' + cutRegion);
  return cutRegion;
}

async function CropCity (city) {
  let cutCity;
  if (city.includes('аул')) {
    cutCity = await city.slice(3).trim();
    if (cutCity.includes('[')) {
      let splitCity = await cutCity.split(' ');
      cutCity = splitCity[0];
    }
  } else {
    if (city.includes('г') || city.includes('с', 0) || city.includes('п', 0) || city.includes('д', 0) || city.includes('х', 0)) {
      cutCity = await city.slice(1).trim();
      if (cutCity.includes('[')) {
        let splitCity = await cutCity.split(' ');
        cutCity = splitCity[0];
      }
    } else {
      await console.log('NOT FOUND REGION');
      return;
    }
  }
  console.log('OLD VALUE: ' + city);
  console.log('NEW VALUE: ' + cutCity);
  return cutCity;
}

async function ExitFromAccount (driver) {
  await driver.findElement(By.xpath(LogoutFieldXpath)).click();
}

/**
 * @return {string}
 */
async function CreateTransactionID () {
  let date = new Date();
  let month = await date.getMonth() + 1;
  let transactionID = await date.getDate() + '' + month + '' + date.getFullYear() + '' + date.getHours() + '' + date.getMinutes() + '' + date.getSeconds();
  return transactionID;
}

async function CreateEmailForPolicySending () {
  await console.log('CREATING EMAIL FOR POLICY SENDING');
  let createdEmail;
  await axios({
    method: 'get',
    url: 'http://82.196.0.152/api1/emails/create',
  }).then(async function (response) {
    const { data: { email } } = response;
    await console.log('EMAIL FOR POLICY: ', email);
    createdEmail = await email;
  }).catch(async function () {
    await console.log('ERROR, EMAIL NOT CREATED');
  });
  return createdEmail;
}


async function ChangeTaskItemValue (taskDB, id, email, status, projectNum, contractNum, coefTable, cost, msg, errName, err, paymentLink, samplePDF, policyPDF, failureData1, failureData2) {
  let baseID = await taskDB.get(`task${id}.id`);
  let baseEmail = await taskDB.get(`task${id}.email`);
  let baseProjectNum = await taskDB.get(`task${id}.projectNumber`);
  let baseContractNum = await taskDB.get(`task${id}.contractNumber`);
  let baseCoefficientTable = await taskDB.get(`task${id}.coefficientTable`);
  let baseCost = await taskDB.get(`task${id}.cost`);
  let baseMessage = await taskDB.get(`task${id}.message`);
  let baseStatus = await taskDB.get(`task${id}.status`);
  let baseError = await taskDB.get(`task${id}.error`);
  let baseErrorName = await taskDB.get(`task${id}.error.name`);
  let baseErrorBody = await taskDB.get(`task${id}.error.body`);
  let baseFailureData1 = await taskDB.get(`task${id}.error.failureData1`);
  let baseFailureData2 = await taskDB.get(`task${id}.error.failureData2`);
  let baseExtra = await taskDB.get(`task${id}.extra`);
  let basePaymentLink = await taskDB.get(`task${id}.extra.paymentLink`);
  let baseSamplePDF = await taskDB.get(`task${id}.extra.samplePDF`);
  let basePolicyPDF = await taskDB.get(`task${id}.extra.policyPDF`);

  await taskDB.set(`task${id}`, {
    id: (!!id && id !== baseID) ? id : (!!baseID) ? baseID : undefined,
    email: (!!email && email !== baseEmail) ? email : (!!baseEmail) ? baseEmail : undefined,
    projectNumber: (!!projectNum && projectNum !== baseProjectNum) ? projectNum : (!!baseProjectNum) ? baseProjectNum : undefined,
    contractNumber: (!!contractNum && contractNum !== baseContractNum) ? contractNum : (!!baseContractNum) ? baseContractNum : undefined,
    coefficientTable: (!!coefTable && coefTable !== baseCoefficientTable) ? coefTable : (!!baseCoefficientTable) ? baseCoefficientTable : undefined,
    cost: (!!cost && cost !== baseCost) ? cost : (!!baseCost) ? baseCost : undefined,
    message: (!!msg && msg !== baseMessage) ? msg : (!!baseMessage) ? baseMessage : undefined,
    status: (!!status && status !== baseStatus) ? status : (!!baseStatus) ? baseStatus : undefined,
    error: (baseStatus === 'error' || !!status && status === 'error') ? {
      name: (!!errName && errName !== baseErrorName) ? errName : (!!baseErrorName) ? baseErrorName : undefined,
      data: (!!err && err !== baseErrorBody) ? await err : (!!baseErrorBody) ? baseErrorBody : undefined
    } : (!!baseError && baseStatus === 'error') ? baseError : undefined,
    extra: (!!paymentLink && paymentLink !== basePaymentLink || !!samplePDF && samplePDF !== baseSamplePDF || !!policyPDF && policyPDF !== basePolicyPDF) ? {
      failureData1: (!!failureData1 && failureData1 !== baseFailureData1) ? failureData1 : (!!baseFailureData1) ? baseFailureData1 : undefined,
      failureData2: (!!failureData2 && failureData2 !== baseFailureData2) ? failureData2 : (!!baseFailureData2) ? baseFailureData2 : undefined,
      paymentLink: (!!paymentLink && paymentLink !== basePaymentLink) ? paymentLink : (!!basePaymentLink) ? basePaymentLink : undefined,
      samplePDF: (!!samplePDF && samplePDF !== baseSamplePDF) ? samplePDF : (!!baseSamplePDF) ? baseSamplePDF : undefined,
      policyPDF: (!!policyPDF && policyPDF !== basePolicyPDF) ? policyPDF : (!!basePolicyPDF) ? basePolicyPDF : undefined
    } : (!!baseExtra) ? baseExtra : undefined,
  });
}

app.route('/v2/policy/create').post(
  async (request, response) => {
    let transactionID = await CreateTransactionID();
    let createdEmail = await CreateEmailForPolicySending();
    await ChangeTaskItemValue(taskDB, transactionID, createdEmail, 'processing');
    await response.status(200).send(taskDB.get(`task${transactionID}`));
    await createDriver();
    await FillingQueue.add(async () => {
      await Authorization(driver).then(async () => {
        await GoToFilling(driver).then(async () => {
          await Filling(driver);
        });
      });
    });
  }
);

app.route('/v2/policy/check-transaction-status').get(
  async (request, response) => {
    await CheckingStatusQueue.add(async () => {
      try {
        const { id } = await request.body;
        let status = await taskDB.get(`task${id}.status`);
        if (status === 'error')
          await response.status(404).send(await taskDB.get(`task${id}`));
        else
          await response.status(200).send(await taskDB.get(`task${id}`));
      } catch (e) {
        await response.status(404).send('ID NOT FOUND IN BASE');
      }
    });
  }
);

function createDriver () {
  let options = new chrome.Options();
  options.addArguments('--disable-dev-shm-usage');
  options.addArguments('--no-sandbox');
  // options.addArguments('--headless');
  options.addArguments('--disable-gpu');
  options.addArguments('--window-size=1280,960');
  options.addArguments('--window-position=490,90');
  options.addArguments('--disable-popup-blocking');
  options.addArguments('--ignore-certificate-errors');

  driver = new webdriver.Builder()
    .forBrowser('chrome')
    .setChromeOptions(options)
    .build();
  console.log('Driver was created');
}

function destroyDriver () {
  driver.quit().then(r => {});
  driver = undefined;
}

app.listen(port, function () {
  console.log(appName + ' started on ' + port + ' port');
});

process.on('exit', function () {
  driver && driver.quit();
});
